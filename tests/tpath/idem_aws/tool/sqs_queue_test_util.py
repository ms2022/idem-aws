import copy
import time
from typing import Dict


async def assert_sqs_queue(
    hub, ctx, initial_attributes: Dict, updated_attributes: Dict, updated_tags: Dict
):
    """
    Tests the creating, updating, describing and deleting of an SQS queue

    Args:
        hub: required for functions in hub
        ctx(Dict):
        initial_attributes(Dict): the initial attributes for creating a queue,
            including idem resource name, queue name, resource_id and tags
        updated_attributes(Dict): the updated attributes for updating the queue,
            including idem resource name, queue name, resource_id and tags
        updated_tags(Dict): the tags to be updated for the queue

    Returns: None
    """

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    name = initial_attributes.get("name")
    attributes = copy.deepcopy(initial_attributes)

    # Create a queue with --test
    expected_new_state = copy.deepcopy(attributes)
    expected_new_state["resource_id"] = "resource_id_known_after_present"

    ret = await hub.states.aws.sqs.queue.present(test_ctx, **attributes)
    assert ret["result"], ret["comment"]
    assert f"Would create aws.sqs.queue '{name}'" in ret["comment"]
    assert not ret.get("old_state")
    assert ret.get("new_state")
    assert ret.get("new_state") == expected_new_state

    # Create a queue
    ret = await hub.states.aws.sqs.queue.present(ctx, **attributes)
    assert ret["result"], ret["comment"]
    assert f"Created aws.sqs.queue '{name}'" in ret["comment"]
    assert not ret.get("old_state")
    assert ret.get("new_state")
    resource_id = ret["new_state"].get("resource_id")
    assert resource_id
    expected_new_state["resource_id"] = resource_id
    arn = ret["new_state"].get("arn")
    assert arn
    expected_new_state["arn"] = arn
    assert ret.get("new_state") == expected_new_state

    # Now that we have the resource_id, update the attributes with it so that it
    # can be used in tests below
    attributes["resource_id"] = resource_id
    updated_attributes["resource_id"] = resource_id

    # Try creating the queue again
    ret = await hub.states.aws.sqs.queue.present(ctx, **attributes)
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") == ret.get("new_state")
    assert ret.get("new_state") == expected_new_state
    assert f"aws.sqs.queue '{name}' already exists" in ret.get("comment")

    describe_ret = await hub.tool.sqs_queue_test_util.get_queue_describe_ret(
        ctx, name, False
    )
    assert describe_ret

    described_resource = describe_ret.get("aws.sqs.queue.present")
    assert described_resource

    expected_describe = []
    for key, value in attributes.items():
        if key != "name":
            expected_describe.append({key: value})
    expected_describe.append({"arn": arn})
    assert hub.tool.aws.state_comparison_utils.are_lists_identical(
        expected_describe, described_resource
    )

    # Update queue attributes with --test
    expected_old_state = copy.deepcopy(attributes)
    expected_old_state["arn"] = arn
    expected_new_state = copy.deepcopy(updated_attributes)

    ret = await hub.states.aws.sqs.queue.present(test_ctx, **updated_attributes)
    assert ret.get("result"), ret.get("comment")
    assert ret.get("old_state") == expected_old_state
    assert ret.get("new_state") == expected_new_state
    assert f"Would update aws.sqs.queue '{name}'" in ret.get("comment")

    # Update queue attributes
    expected_new_state["arn"] = arn
    ret = await hub.states.aws.sqs.queue.present(ctx, **updated_attributes)
    assert ret.get("result"), ret.get("comment")
    assert ret.get("old_state") == expected_old_state
    assert ret.get("new_state") == expected_new_state
    assert f"Updated aws.sqs.queue '{name}'" in ret.get("comment")

    # Try updating attributes again with no changes
    ret = await hub.states.aws.sqs.queue.present(ctx, **updated_attributes)
    assert ret.get("result"), ret.get("comment")
    assert ret.get("old_state") == ret.get("new_state")
    assert ret.get("new_state") == expected_new_state
    assert f"aws.sqs.queue '{name}' already exists" in ret.get("comment")

    attributes = copy.deepcopy(updated_attributes)

    # Add and update queue tags with --test
    attributes_with_new_tags = copy.deepcopy(attributes)
    attributes_with_new_tags["tags"] = updated_tags

    expected_old_state = copy.deepcopy(attributes)
    expected_old_state["arn"] = arn
    expected_new_state = copy.deepcopy(attributes_with_new_tags)

    ret = await hub.states.aws.sqs.queue.present(test_ctx, **attributes_with_new_tags)
    assert ret.get("result"), ret.get("comment")
    assert ret.get("old_state") == expected_old_state
    assert ret.get("new_state") == expected_new_state

    # When the tag comment is constructed in present, the dict old_state["tags"] is used to determine the tags
    # that need to be added, updated or removed. The items of old_state["tags"] might not be
    # in the same order every time they are returned from AWS or Localstack,
    # so it is possible that the keys of old_state["tags"] and expected_old_state["tags"] to not be in the same order.
    # To guarantee that the order of the tags in the expected comment is the same as in the real comment,
    # ret["old_state"]["tags"] is used for constructing the expected comment
    # everywhere the method get_expected_tag_comment is called.
    expected_comment = hub.tool.sqs_queue_test_util.get_expected_tag_comment(
        expected_new_state["tags"], ret["old_state"]["tags"], True
    )
    assert expected_comment in ret.get("comment")

    # Add and update queue tags
    expected_new_state["arn"] = arn
    ret = await hub.states.aws.sqs.queue.present(ctx, **attributes_with_new_tags)
    assert ret.get("result"), ret.get("comment")
    assert ret.get("old_state") == expected_old_state
    assert ret.get("new_state") == expected_new_state

    expected_comment = hub.tool.sqs_queue_test_util.get_expected_tag_comment(
        expected_new_state["tags"], ret["old_state"]["tags"], False
    )
    assert expected_comment in ret.get("comment")

    # Add and update queue tags again with no changes
    ret = await hub.states.aws.sqs.queue.present(ctx, **attributes_with_new_tags)
    assert ret.get("result"), ret.get("comment")
    assert ret.get("old_state") == ret.get("new_state")
    assert ret.get("new_state") == expected_new_state
    assert f"aws.sqs.queue '{name}' already exists" in ret.get("comment")

    # Remove queue tags with --test
    attributes_with_removed_tags = copy.deepcopy(attributes_with_new_tags)
    attributes_with_removed_tags["tags"] = {}

    expected_old_state = copy.deepcopy(attributes_with_new_tags)
    expected_old_state["arn"] = arn
    expected_new_state = copy.deepcopy(attributes_with_removed_tags)

    ret = await hub.states.aws.sqs.queue.present(
        test_ctx, **attributes_with_removed_tags
    )
    assert ret.get("result"), ret.get("comment")
    assert ret.get("old_state") == expected_old_state
    assert ret.get("new_state") == expected_new_state

    expected_comment = hub.tool.sqs_queue_test_util.get_expected_tag_comment(
        expected_new_state["tags"], ret["old_state"]["tags"], True
    )
    assert expected_comment in ret.get("comment")

    # Remove queue tags
    expected_new_state["arn"] = arn
    ret = await hub.states.aws.sqs.queue.present(ctx, **attributes_with_removed_tags)
    assert ret.get("result"), ret.get("comment")
    assert ret.get("old_state") == expected_old_state
    assert ret.get("new_state") == expected_new_state

    expected_comment = hub.tool.sqs_queue_test_util.get_expected_tag_comment(
        expected_new_state["tags"], ret["old_state"]["tags"], False
    )
    assert expected_comment in ret.get("comment")

    # Remove queue tags again with no changes
    ret = await hub.states.aws.sqs.queue.present(ctx, **attributes_with_removed_tags)
    assert ret.get("result"), ret.get("comment")
    assert ret.get("old_state") == ret.get("new_state")
    assert ret.get("new_state") == expected_new_state
    assert f"aws.sqs.queue '{name}' already exists" in ret.get("comment")

    # Delete the queue with --test
    ret = await hub.states.aws.sqs.queue.absent(test_ctx, name, resource_id)
    assert ret.get("result"), ret.get("comment")
    assert ret.get("old_state")
    assert not ret.get("new_state")
    assert f"Would delete aws.sqs.queue '{name}'" in ret.get("comment")

    # Delete the queue
    ret = await hub.states.aws.sqs.queue.absent(ctx, name, resource_id)
    assert ret.get("result"), ret.get("comment")
    assert ret.get("old_state")
    assert not ret.get("new_state")
    assert f"Deleted aws.sqs.queue '{name}'" in ret.get("comment")

    # Try deleting the queue again
    ret = await hub.states.aws.sqs.queue.absent(ctx, name, resource_id)
    assert ret.get("result"), ret.get("comment")
    assert not ret.get("old_state")
    assert not ret.get("new_state")
    assert f"aws.sqs.queue '{name}' already absent" in ret.get("comment")

    # Try describing the deleted queue
    describe_ret = await hub.tool.sqs_queue_test_util.get_queue_describe_ret(
        ctx, name, True
    )
    assert not describe_ret


async def get_queue_describe_ret(hub, ctx, name: str, deleted_queue: bool) -> Dict:
    """
    Tries to describe an SQS queue

    Args:
        hub:
        ctx:
        name: the name of the SQS queue

    Returns: the described queue dict object

    """
    # describe() uses the boto3 client method list_queues(). If describe() is called too soon with the real AWS
    # after creating or deleting a queue, sometimes it might not return the most up-to-date
    # list of queue URLs, i.e. the URLs of already deleted queues might still be included in
    # the list or the URLs of newly created queues might not be added to the list yet,
    # which is why it is needed to retry calling describe() until it returns the correct URLs
    max_retries = 45
    delay = 3
    describe_ret = {}
    for i in range(max_retries):
        # Describe the queue
        ret = await hub.states.aws.sqs.queue.describe(ctx)

        # describe uses queue_name as idem resource name when
        # it returns the resource which is why we use it here to check
        # if describe has returned the correct sqs queue resource
        describe_ret = ret.get(name)
        if describe_ret:
            break
        elif deleted_queue:
            break
        time.sleep(delay)

    return describe_ret


def get_expected_tag_comment(hub, new_tags: Dict, old_tags: Dict, test: bool) -> str:
    """
    Generates the expected comment that should be returned from present when adding, updating or deleting resource tags

    Args:
        hub(dict): the idem hub
        new_tags(Dict): the new tags of the resource
        old_tags(Dict): the old tags of the resource
        test(bool): is present run with the --test flag

    Returns: the expected comment string
    """
    tags_to_remove, tags_to_add = hub.tool.aws.tag_utils.diff_tags_dict(
        old_tags, new_tags
    )

    if test:
        return hub.tool.aws.comment_utils.would_update_tags_comment(
            tags_to_remove, tags_to_add
        )[0]

    return hub.tool.aws.comment_utils.update_tags_comment(tags_to_remove, tags_to_add)[
        0
    ]

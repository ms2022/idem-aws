import asyncio
import copy
import time
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_certificate_manager_import, cleanup):
    global PARAMETER
    ctx["test"] = __test
    name = aws_certificate_manager_import["domain_name"]
    PARAMETER["name"] = name
    regional_certificate_arn = aws_certificate_manager_import["resource_id"]
    PARAMETER["regional_certificate_arn"] = regional_certificate_arn
    PARAMETER["endpoint_configuration"] = {
        "types": ["REGIONAL"],
    }
    PARAMETER["security_policy"] = "TLS_1_2"
    PARAMETER["tags"] = {
        "idem-resource-name": name,
        "time": str(int(time.time())),
    }
    comment_utils_kwargs = {
        "resource_type": "aws.apigateway.domain_name",
        "name": name,
    }
    created_message = hub.tool.aws.comment_utils.create_comment(**comment_utils_kwargs)
    would_create_message = hub.tool.aws.comment_utils.would_create_comment(
        **comment_utils_kwargs
    )
    ret = await hub.states.aws.apigateway.domain_name.present(
        ctx,
        **PARAMETER,
    )
    assert ret["result"], ret["comment"]
    created_resource = ret["new_state"]
    if __test:
        assert would_create_message == ret["comment"]
    else:
        PARAMETER["resource_id"] = created_resource["resource_id"]
        assert created_message == ret["comment"]

    assert not ret["old_state"] and ret["new_state"]
    assert PARAMETER["name"] == created_resource.get("name")
    assert PARAMETER["regional_certificate_arn"] == created_resource.get(
        "regional_certificate_arn"
    )
    assert PARAMETER["endpoint_configuration"] == created_resource.get(
        "endpoint_configuration"
    )
    assert PARAMETER["security_policy"] == created_resource.get("security_policy")
    assert PARAMETER["tags"] == created_resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.apigateway.domain_name.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "aws.apigateway.domain_name.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id]["aws.apigateway.domain_name.present"]
    described_resource_map = dict(ChainMap(*described_resource))
    assert described_resource_map["tags"] == PARAMETER["tags"]
    assert (
        described_resource_map["regional_certificate_arn"]
        == PARAMETER["regional_certificate_arn"]
    )
    assert (
        described_resource_map["endpoint_configuration"]
        == PARAMETER["endpoint_configuration"]
    )
    assert described_resource_map["security_policy"] == PARAMETER["security_policy"]
    assert described_resource_map["domain_name"] == PARAMETER["name"]


@pytest.mark.asyncio
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="exec-get", depends=["present"])
# This test is here to avoid the need to create a domain_name fixture for exec.get() testing
async def test_get(hub, ctx):
    ret = await hub.exec.aws.apigateway.domain_name.get(
        ctx=ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["name"] == resource.get("domain_name")
    assert PARAMETER["endpoint_configuration"] == resource.get("endpoint_configuration")
    assert PARAMETER["regional_certificate_arn"] == resource.get(
        "regional_certificate_arn"
    )
    assert PARAMETER["tags"] == resource.get("tags")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="update_domain_name", depends=["describe"])
async def test_update_domain_name(hub, ctx, __test):
    # this is to avoid a "TooManyRequestsException" thrown by the API Gateway servers
    # (request is being made too frequently and is more than what the server can handle)
    if not hub.tool.utils.is_running_localstack(ctx):
        await asyncio.sleep(30)
    global PARAMETER
    new_parameter = copy.deepcopy(PARAMETER)
    ctx["test"] = __test
    new_parameter["security_policy"] = "TLS_1_0"
    if not hub.tool.utils.is_running_localstack(ctx):
        # LocalStack does not have support for apigateway tags
        new_parameter["tags"] = {
            "updating_idem-resource-tag-1": PARAMETER["name"],
            "time": str(int(time.time())),
        }
    ret = await hub.states.aws.apigateway.domain_name.present(ctx, **new_parameter)
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["resource_id"] == old_resource.get("resource_id")
    assert PARAMETER["regional_certificate_arn"] == old_resource.get(
        "regional_certificate_arn"
    )
    assert PARAMETER["name"] == old_resource.get("domain_name")
    assert PARAMETER["endpoint_configuration"] == old_resource.get(
        "endpoint_configuration"
    )
    assert PARAMETER["tags"] == old_resource.get("tags")
    resource = ret["new_state"]
    assert new_parameter["name"] == resource.get("domain_name")
    assert new_parameter["resource_id"] == resource.get("resource_id")
    assert new_parameter["security_policy"] == resource.get("security_policy")
    assert new_parameter["endpoint_configuration"] == resource.get(
        "endpoint_configuration"
    )
    assert new_parameter["tags"] == resource.get("tags")
    if not __test:
        PARAMETER = new_parameter


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="absent", depends=["update_domain_name"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.domain_name.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret["old_state"] and not ret["new_state"]
    old_resource = ret["old_state"]
    assert PARAMETER["name"] == old_resource.get("name")
    assert PARAMETER["resource_id"] == old_resource.get("resource_id")
    assert PARAMETER["security_policy"] == old_resource.get("security_policy")
    assert PARAMETER["security_policy"] == old_resource.get("security_policy")
    assert PARAMETER["regional_certificate_arn"] == old_resource.get(
        "regional_certificate_arn"
    )
    assert PARAMETER["endpoint_configuration"] == old_resource.get(
        "endpoint_configuration"
    )

    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.apigateway.domain_name", name=PARAMETER["name"]
            )[0]
            == ret["comment"][0]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.apigateway.domain_name", name=PARAMETER["name"]
            )[0]
            == ret["comment"][0]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.domain_name.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigateway.domain_name", name=PARAMETER["name"]
        )
        == ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.apigateway.domain_name.absent(
            ctx,
            name=PARAMETER["name"],
            resource_id=PARAMETER["resource_id"],
        )
